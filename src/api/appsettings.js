import { StoreGlobal } from '../features/GlobalState';

const appsettings = async () => {
	
	const response = await fetch("http://northsightwebservice-prod.us-west-2.elasticbeanstalk.com/api/appsettings", {
		method: "GET",
		headers: {
			'Content-Type': 'application/json',
			'Content-Encoding': 'gzip'
		},
		body: null
	});
	const responseJson = await response.json();
    return responseJson;
};

export {
	appsettings
}