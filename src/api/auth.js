import { StoreGlobal } from '../features/GlobalState';

const login = async () => {
	// Make API call to retrieve an access token
	var response = await userLogin();
	if (response != undefined && response.instance_url !== undefined && response.access_token !== undefined) {
		StoreGlobal({
			type: 'set',
			key: 'admin_instance_url',
			value: response.instance_url
		})
		StoreGlobal({
			type: 'set',
			key: 'admin_access_token',
			value: response.access_token
		})
		response = '';
	} else {
		response = 'Invalid username or password.';
	}
	return response;
}

const isLoggedIn = async () => {
	return !!StoreGlobal({type:'get',key:'admin_access_token'});
}

const logout = async () => {
	await StoreGlobal({
		type: 'set',
		key: 'admin_instance_url',
		value: null
	})
	await StoreGlobal({
		type: 'set',
		key: 'admin_access_token',
		value: null
	})
	return StoreGlobal({type:'get',key:'access_token'});
}

userLogin = async () => {
	//alert('UserLogin function');
	var params = {
		grant_type: 'password',
		client_id: '3MVG9iTxZANhwHQtX3UhBKfL5_x34fPnxJdzLeDwrIsxhMmbXkaIP7_ilVylf0C1tbUA.zG_raHb5ktuNObOx',
		client_secret: '1334697778316286730',
		username: 'mobileapp@northsight.com',
		password: 'T3g[pUAN:yrJ6ubS'
	};

	var urlParams = [];
	for (var property in params) {
		var encodedKey = encodeURIComponent(property);
		var encodedValue = encodeURIComponent(params[property]);
		urlParams.push(encodedKey + "=" + encodedValue);
	}
	urlParams = urlParams.join("&");

	const settings = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
		},
		body: null
	};
	//alert("https://login.salesforce.com/services/oauth2/token?" + urlParams, settings);
	const data = await fetch("https://login.salesforce.com/services/oauth2/token?" + urlParams, settings)
		.catch(err => {
			return 'Invalid username or password.';
		});

	if (data.ok)
		return data.json();
	else
		return data;
}

export {
	login,
	isLoggedIn,
	logout
}