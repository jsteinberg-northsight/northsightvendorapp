import { PermissionsAndroid, Platform } from 'react-native';

const perm = async (permissionType) => {
	
	if (Platform.OS === 'android' && Platform.Version >= 23) {
		const granted = await PermissionsAndroid.request(permissionType);
		return granted;
	}
	else
		return PermissionsAndroid.RESULTS.GRANTED;
};

export {
	perm
}