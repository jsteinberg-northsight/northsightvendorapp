import { StoreGlobal } from '../features/GlobalState';
import { login } from '../api/auth'

const query = async (queryString, callType) => {
	
	var encoded = encodeURIComponent(queryString);
	if (StoreGlobal({type:'get',key:'admin_instance_url'}) === undefined || StoreGlobal({type:'get',key:'admin_instance_url'}) === null)
		await login();

	if (callType === 'admin'){
		var instance_url = StoreGlobal({type:'get',key:'admin_instance_url'});
		var access_token = StoreGlobal({type:'get',key:'admin_access_token'});
	}
	else {
		var instance_url = StoreGlobal({type:'get',key:'instance_url'});
		var access_token = StoreGlobal({type:'get',key:'access_token'});
	}

	const response = await fetch(instance_url + "/services/data/v44.0/query?q=" + encoded, {
		method: "GET",
		headers: {
			'Content-Type': 'application/json',
			'Content-Encoding': 'gzip',
			'Authorization': 'Bearer ' + access_token
		},
		body: null
	});
	const responseJson = await response.json();
    return responseJson;
};

export {
	query
}