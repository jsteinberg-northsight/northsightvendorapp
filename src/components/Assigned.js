import React, { Component } from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, StyleSheet, ActivityIndicator, FlatList } from 'react-native'
import { NavigationEvents } from 'react-navigation';
import { query } from '../api/query';
import { StoreGlobal } from '../features/GlobalState';

export default class AssignedComponent extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			dataSource: [],
			isLoading: false
		}
	}

	componentDidMount() {
		
	}

	_getProperties = async (e) => {

		this.setState({isLoading: true});
		//const userID = StoreGlobal({type:'get',key:'sfdc_community_id'});
		var queryString = `SELECT Id, Geocode_Cache__r.Id, Geocode_Cache__r.Location__Latitude__s, Geocode_Cache__r.Location__Longitude__s, Geocode_Cache__r.Formatted_Street_Address__c,
			Geocode_Cache__r.Formatted_City__c, Geocode_Cache__r.Formatted_State__c, Geocode_Cache__r.Formatted_Zip_Code__c, Scheduled_Date__c, Work_Ordered__c, CaseNumber FROM Case 
			WHERE Serviceable_Text__c = 'Yes'`;
		var response = await query(queryString, 'admin');

		var orderMarkers = [];

		for (let i = 0; i < response.records.length; i++) {
			orderMarkers.push({due: response.records[i].Scheduled_Date__c, key: response.records[i].Id, coordinate: {latitude: response.records[i].Geocode_Cache__r.Location__Latitude__s, 
				longitude: response.records[i].Geocode_Cache__r.Location__Longitude__s }, ordered: response.records[i].Work_Ordered__c, wonum: response.records[i].CaseNumber,
				title: `${response.records[i].Geocode_Cache__r.Formatted_Street_Address__c}, ${response.records[i].Geocode_Cache__r.Formatted_City__c}, ${response.records[i].Geocode_Cache__r.Formatted_State__c} ${response.records[i].Geocode_Cache__r.Formatted_Zip_Code__c}`});
		}

		this.setState({dataSource: orderMarkers, isLoading: false});
	}

	_propertySelected = (property) => {
		this.props.navigation.navigate('Details', { property });
	}

	render() {
		return (
			<SafeAreaView style={{flex: 1}}>
				<View style={styles.container} >
					{this.state.isLoading && <ActivityIndicator />}
					<NavigationEvents onDidFocus={() => this._getProperties()} />
					<FlatList
						style={{flex: 1, width: '100%'}}
						data={this.state.dataSource}
						renderItem={({item}) => {
							return(
							<TouchableOpacity style={{borderColor: 'black', borderWidth: 1}} onPress={() => this._propertySelected(item.key)}>
							<Text style={[styles.headers, { color: 'green' }]}>Order #: {item.wonum}</Text>
							<Text style={styles.headers}>{item.title}</Text>
							<Text style={styles.workordered}>{item.ordered}</Text>
							<Text style={styles.due}>Due: {item.due}</Text>
							</TouchableOpacity>)}}
						keyExtractor={item => item.key}
					/>
				</View>
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'flex-start'
	},
	headers: {
		fontWeight: 'bold',
		color: 'black',
		fontSize: 17
	},
	workordered: {
		color: 'blue'
	},
	due: {
		color: 'red'
	}
})