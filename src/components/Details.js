import React, { Component } from 'react'
import { Text, View, SafeAreaView, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native'
import { NavigationEvents } from 'react-navigation';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import { query } from '../api/query';

export default class DetailsComponent extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			address: '',
			due: '',
			tasks: [],
			isLoading: false
		}
	}

	componentDidMount(){

	}

	_getDetails = async (e) => {

		const { navigation } = this.props;
		const woID = navigation.getParam('property');
		
		this.setState({isLoading: true});
		var queryString = `SELECT Id, Geocode_Cache__r.Id, Geocode_Cache__r.Location__Latitude__s, Geocode_Cache__r.Location__Longitude__s, Geocode_Cache__r.Formatted_Street_Address__c,
			Geocode_Cache__r.Formatted_City__c, Geocode_Cache__r.Formatted_State__c, Geocode_Cache__r.Formatted_Zip_Code__c, Scheduled_Date__c, Work_Ordered__c, CaseNumber FROM Case 
			WHERE Id = '${woID}'`;
		var response = await query(queryString, 'admin');

		this.setState({address: `${response.records[0].Geocode_Cache__r.Formatted_Street_Address__c}, 
		${response.records[0].Geocode_Cache__r.Formatted_City__c}, ${response.records[0].Geocode_Cache__r.Formatted_State__c} ${response.records[0].Geocode_Cache__r.Formatted_Zip_Code__c}`,
		due: response.records[0].Scheduled_Date__c});
		this.setState({isLoading: false});
	}

	render() {	
		return (
			<SafeAreaView style={{flex: 1}}>
				<View style={styles.container}>
					{this.state.isLoading && <ActivityIndicator />}
					<NavigationEvents onDidFocus={() => this._getDetails()} />
					<View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
						<Text style={{alignSelf: 'flex-start', textAlign: 'left', flex: 1}}>Status: Assigned</Text><Text style={{alignSelf: 'flex-end', textAlign: 'right', flex: 1}}>Due: {this.state.due}</Text>
					</View>
					<Text>{this.state.address}</Text>
					<Text>Map goes here</Text>
					<TouchableOpacity style={styles.linkContainer} 
						onPress={this.onLogin}
						disabled={this.state.isLoggingIn} >
						<Text style={styles.linkText}>Project Instructions</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.buttonContainer}
						onPress={this.onLogin}
						disabled={this.state.isLoggingIn} >
						<Text style={styles.buttonText}>Order Complete</Text>
					</TouchableOpacity>
					<Text>Task List</Text>
				</View>
			</SafeAreaView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'flex-start',
	},
	headers: {
		fontWeight: 'bold',
		color: 'black',
		fontSize: 17
	},
	workordered: {
		color: 'blue'
	},
	due: {
		color: 'red'
	}
})