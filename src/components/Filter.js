import React, { Component } from 'react'
import { View, StyleSheet, Picker, Dimensions } from 'react-native'
import { StoreGlobal } from '../features/GlobalState';

export default class FilterComponent extends Component {
	
	constructor(props) {
		super(props);
	}

	componentDidMount(){
        this._retrieveData();
	}
	
	state = {
        cities: ["Filter by City"],
        zip_codes: ["Filter by Zip Code"],
        payouts: ["Filter by Payout"]
	}

	_retrieveData = async () => {
		try {
			var cities = await StoreGlobal({type:'get',key:'cities'});
			var zip_codes = await StoreGlobal({type:'get',key:'zip_codes'});
			var payouts = await StoreGlobal({type:'get',key:'payouts'});
			if (cities !== null)
				this.setState({ cities: cities });
			if (zip_codes !== null)
				this.setState({ zip_codes: zip_codes });
			if (payouts !== null)
				this.setState({ payouts: payouts });
		} catch (error) {
			alert(error);
		}
	}

	render() {
		return (
			<View>
				<Picker>
				{this.state.cities.map((l, i) => {return <Picker.Item value={l} label={l} key={i}  /> })}
				</Picker>
				<Picker>
				{this.state.zip_codes.map((l, i) => {return <Picker.Item value={l} label={l} key={i}  /> })}
				</Picker>
				<Picker>
				{this.state.payouts.map((l, i) => {return <Picker.Item value={l} label={l} key={i}  /> })}
				</Picker>				
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#B5D1FF',
		alignItems: 'center',
		justifyContent: 'center',
	},
	input: {
		height: 40,
		textAlign: 'center',
		backgroundColor: 'rgba(225,225,225,0.4)',
		marginBottom: 10,
		padding: 10,
		color: '#000',
		width: '50%'
	},
	image: {
		resizeMode: 'contain',
		width: Dimensions.get('window').width * .9,
	},
	buttonContainer: {
		backgroundColor: '#2980b6',
		paddingVertical: 15,
		width: '50%'
	},
	buttonText: {
		color: '#fff',
		textAlign: 'center',
		fontWeight: '700'
	}
})