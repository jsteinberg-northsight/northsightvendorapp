import React, { Component } from 'react'
import { KeyboardAvoidingView, View, Image, StyleSheet, ActivityIndicator, Dimensions } from 'react-native'
import { WebView } from 'react-native-webview'
import Store from 'react-native-simple-store';
import { StoreGlobal } from '../features/GlobalState';

const {width, height} = Dimensions.get('window');

export default class FirstTimeAuthComponent extends Component {
	
	constructor(props) {
		super(props);
	}

	static navigationOptions = {
		headerMode: null
	}

	onLogin = async () => {
		
		this.setState({
			isLoggingIn: true
		});

		this.state.injectScript = `
			(function () {
				var elementExists = document.getElementById("oaapprove");
				if (elementExists !== null)
					document.getElementById("oaapprove").click();
				}());
		`;

		this.state.url = "https://northsight.force.com/vendors/services/oauth2/authorize?response_type=token&client_id=3MVG9iTxZANhwHQtX3UhBKfL5_x34fPnxJdzLeDwrIsxhMmbXkaIP7_ilVylf0C1tbUA.zG_raHb5ktuNObOx&redirect_uri=NSVendor://";
	}

	componentDidMount(){
		this.onLogin();
	}
	
	state = {
		isLoggingIn: false,
		url: 'about:blank',
		injectScript: ''
	}

	_onNavigationStateChange = (event) => {
		if (event.url.indexOf("nsvendor://#") > -1) {
		  this._webview.stopLoading();
		  const route = event.url.split('nsvendor://#')[1].split('&');
	
		  let queryParams = {};
		  route.forEach(param => {
			let s = param.split('=');
			queryParams[s[0]] = s[1];
			});
			
		  StoreGlobal({
				type: 'set',
				key: 'instance_url',
				value: decodeURIComponent(queryParams.instance_url)
			})
			StoreGlobal({
				type: 'set',
				key: 'access_token',
				value: decodeURIComponent(queryParams.access_token)
			})
			
			this.props.navigation.navigate('Orders');
		}
	}

	render() {
		return (
			<KeyboardAvoidingView behavior="padding" style={styles.container} >

				<WebView
					ref={component => this._webview = component}
					javaScriptEnabled={true}
					onNavigationStateChange={this._onNavigationStateChange}
					injectedJavaScript = {this.state.injectScript}
					originWhitelist={['*']}
					startInLoadingState={false}
					source={{uri: this.state.url}}
					style={styles.web}
				/>

				{this.state.isLoggingIn && <ActivityIndicator />}
				
			</KeyboardAvoidingView>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'column',
		backgroundColor: '#B5D1FF',
		alignItems: 'center',
		justifyContent: 'center',
		justifyContent: 'space-between'
	},
	web: {
		height: 1,
		width: 1,
		flex: 0
	},
})