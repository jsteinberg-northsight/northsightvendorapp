import React, { Component } from 'react'
import {
  Text,
  TextInput,
  KeyboardAvoidingView,
  View,
  TouchableOpacity,
  Image,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  Modal,
  Switch,
  Linking,
  Platform
} from 'react-native'
import Store from 'react-native-simple-store'
//import CheckBox from 'react-native-checkbox'
import { StoreGlobal } from '../features/GlobalState'
import { WebView } from 'react-native-webview'
import { appsettings } from '../api/appsettings'

const { width, height } = Dimensions.get('window')

export default class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: '',
      isLoggingIn: false,
      message: '',
      checked: true,
      rendered: false,
      url: 'about:blank',
      injectScript: '',
      modalVisible: false,
      loginTried: false,
      loadingText: 'Logging in, this may take a moment...'
    }
  }

  static navigationOptions = {
    headerMode: null
  }

  onForgotPassword = () => {
    if (this.state.username !== ''){
      this.setState({
        isLoggingIn: false,
        loginTried: false,
        url: 'https://northsight.force.com/vendors/secur/forgotpassword.jsp?locale=us&lqs=sfdcIFrameOrigin%3Dnull&display=touch',
        injectScript: `
          (function () {
            var elementExists = document.getElementById("un");
            if (elementExists !== null && elementExists.value === '') {
              document.getElementById("un").value = "${this.state.username}";
              document.getElementById("continue").click();
              document.addEventListener('DOMContentLoaded', function () {
                window.location = "about:blank";
              });
            }}());
        `
      });
      this.setModalVisible(true);
    }
    else
      alert('Please enter your username to continue.');
  }

  onPhonePress(){
    if (Platform.OS !== 'android' && Linking.canOpenURL('telprompt:6028424000'))
      Linking.openURL('telprompt:6028424000');
    else if (Linking.canOpenURL('tel:6028424000'))
      Linking.openURL('tel:6028424000');
    else
      alert('This device does not support passing phone numbers. Please call (602) 842-4000 for assistance.')
  }

  onWebPress(){
    if (Linking.canOpenURL('http://www.northsight.com'))
      Linking.openURL('http://www.northsight.com');
    else
      alert('Opening http://www.northsight.com failed.')
  }

  onLogin = async => {
    this.setState({
      isLoggingIn: true,
      loginTried: false,
      url:
        'https://northsight.force.com/vendors/services/oauth2/authorize?response_type=token&client_id=3MVG9iTxZANhwHQtX3UhBKfL5_x34fPnxJdzLeDwrIsxhMmbXkaIP7_ilVylf0C1tbUA.zG_raHb5ktuNObOx&redirect_uri=NSVendor://',
      injectScript: `
        (function () {
          var elementExists = document.getElementById("oaapprove");
          if (elementExists !== null)
            document.getElementById("oaapprove").click();
          }());
        (function () {
          var elementExists = document.getElementById("username");
          if (elementExists !== null && elementExists.value === '') {
            document.getElementById("username").value = "${this.state.username}";
            document.getElementById("password").value = "${this.state.password}";
            document.getElementById("Login").click();
            document.addEventListener('DOMContentLoaded', function () {
              window.location = "about:blank";
            });
          }}());
        (function () {
          var elementExists = document.getElementById("auraAppcacheProgress");
          if (elementExists !== null)
            window.location = "https://northsight.force.com/vendors/services/oauth2/authorize?response_type=token&client_id=3MVG9iTxZANhwHQtX3UhBKfL5_x34fPnxJdzLeDwrIsxhMmbXkaIP7_ilVylf0C1tbUA.zG_raHb5ktuNObOx&redirect_uri=NSVendor://";
          }());
      `
    })
// 
    if (this.state.checked)
      this._storeData();
    this.setModalVisible(true);
  }

  componentDidMount() {
    this.setState({ rendered: true });
    this._retrieveData();
    this._getAppsettings();
  }

  _getAppsettings = async (e) => {
    var response = await appsettings();
    if (response != undefined && response.MapQueryString !== undefined && response.MapCssString !== undefined) {
      StoreGlobal({
        type: 'set',
        key: 'MapQueryString',
        value: response.MapQueryString
      })
      StoreGlobal({
        type: 'set',
        key: 'MapCssString',
        value: response.MapCssString
      })
    }
  }

  onChangeCheck() {
    this.setState({ checked: !this.state.checked })
    if (this.state.rendered) this._checkBoxChanged();
  }

  _checkBoxChanged = () => {
    if (!this.state.checked) this._storeData();
    else this._clearData();
  }

  _clearData = () => {
    try {
      Store.delete('username');
      Store.delete('password');
    } catch (error) {
      alert(error);
    }
  }

  _storeData = async () => {
    try {
      Store.save('username', this.state.username)
      Store.save('password', this.state.password)
    } catch (error) {
      alert(error)
    }
  }

  _retrieveData = async () => {
    try {
      var user = await Store.get('username')
      var pass = await Store.get('password')
      if (user !== null && pass !== null) {
        // We have data!!
        this.setState({username: user, password: pass});
        this._username.setNativeProps({ text: user });
        this._password.setNativeProps({ text: pass });
      } else {
        this.setState({ checked: false });
      }
    } catch (error) {
      alert(error);
    }
  }

  _onNavigationStateChange = event => {
    if (event.url.indexOf('nsvendor://#') > -1) {
      this._webview.stopLoading();
      this.setModalVisible(false);

      const route = event.url.split('nsvendor://#')[1].split('&');

      let queryParams = {};
      route.forEach(param => {
        let s = param.split('=');
        queryParams[s[0]] = s[1];
      })

      StoreGlobal({
        type: 'set',
        key: 'instance_url',
        value: decodeURIComponent(queryParams.instance_url)
      });
      StoreGlobal({
        type: 'set',
        key: 'access_token',
        value: decodeURIComponent(queryParams.access_token)
      });
      StoreGlobal({
        type: 'set',
        key: 'sfdc_community_id',
        value: decodeURIComponent(queryParams.sfdc_community_id)
      });

      this.props.navigation.navigate('Market');
    }
    else if (event.url === 'https://northsight.force.com/vendors/secur/forgotpassword.jsp'){
      this._webview.stopLoading();
      this.setState({ message: 'Password reset email sent.', isLoggingIn: false });
      this.setModalVisible(false);
    }
    else if (
        event.url === 'https://northsight.force.com/vendors/login'
      ) {
        if (this.state.loginTried === false){
          this.setState({ loginTried: true });
        }
        else{
          this._webview.stopLoading();
          this.setModalVisible(false);
          this.setState({ message: 'Invalid username or password', isLoggingIn: false });
        }
      }
  }

  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <Image
          resizeMode={'cover'}
          style={styles.image}
          source={require('../../assets/images/NorthsightLogo-min.png')}
        />

        <TextInput
          style={styles.input}
          autoCapitalize="none"
          ref={component => (this._username = component)}
          onChangeText={username => this.setState({ username })}
          onSubmitEditing={() => this._password.focus()}
          autoCorrect={false}
          returnKeyType="next"
          placeholder="Salesforce Email"
          placeholderTextColor="rgba(41,128,182,0.6)"
        />

        <TextInput
          style={styles.input}
          autoCapitalize="none"
          ref={component => (this._password = component)}
          returnKeyType="go"
          onChangeText={password => this.setState({ password })}
          placeholder="Salesforce Password"
          placeholderTextColor="rgba(41,128,182,0.6)"
          secureTextEntry
          onSubmitEditing={this._userLogin}
        />

        {!!this.state.message && (
          <Text style={{ fontSize: 14, color: 'red', padding: 5 }}>
            {this.state.message}
          </Text>
        )}
        {this.state.isLoggingIn && <ActivityIndicator />}

        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
            <Switch
              value={this.state.checked}
              onChange={() => this.onChangeCheck()}
              trackColor={this.state.checked ? '#57c2e6':'#ffffff'}
              thumbColor={this.state.checked ? '#42aacd':'#b0adac'}
            />
            <Text> Remember Me? | </Text>
            <TouchableOpacity onPress={this.onForgotPassword} disabled={this.state.isLoggingIn}>
              <Text>Forgot Password?</Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.buttonContainer} onPress={this.onLogin = this.onLogin.bind(this)} disabled={this.state.isLoggingIn}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
          <Image resizeMode={'contain'} style={styles.smallImage} source={require('../../assets/images/webicon.png')} />
          <TouchableOpacity onPress={this.onWebPress}>
            <Text style={{fontSize: 14, fontWeight: '700'}}>   Website</Text>
          </TouchableOpacity>
          <Text style={{fontSize: 14, fontWeight: '700'}}> | </Text>
          <Image resizeMode={'contain'} style={styles.smallImage} source={require('../../assets/images/phoneicon.png')} />
          <TouchableOpacity onPress={this.onPhonePress}>
            <Text style={{fontSize: 14, fontWeight: '700'}}>  Contact Us</Text>
          </TouchableOpacity>
        </View>
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}>
          <View
            style={styles.container}>
            <ActivityIndicator size='large' color='#2980b6' style={styles.activityIndicator} />
            <Text style={styles.loadingText}>{this.state.loadingText}</Text>
            <WebView
              ref={component => (this._webview = component)}
              javaScriptEnabled={true}
              onNavigationStateChange={this._onNavigationStateChange = this._onNavigationStateChange.bind(this)}
              injectedJavaScript={this.state.injectScript}
              originWhitelist={['*']}
              startInLoadingState={false}
              onShouldStartLoadWithRequest={false}
              source={{ uri: this.state.url }}
              style={styles.web}
            />
          </View>
        </Modal>
      </KeyboardAvoidingView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    //backgroundColor: '#B5D1FF',
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center'
  },
  web: {
    height: height,
    width: width,
    flex: 1
  },
  input: {
    height: 40,
    textAlign: 'center',
    backgroundColor: 'rgba(225,225,225,0.4)',
    marginBottom: 10,
    padding: 10,
    color: '#000',
    width: '70%',
    borderRadius: 8
  },
  image: {
    resizeMode: 'contain',
    width: width * 0.8
  },
  smallImage: {
    resizeMode: 'contain',
    height: height * 0.03,
    width: height * .03
  },
  buttonContainer: {
    backgroundColor: '#42aacd',
    paddingVertical: 15,
    width: '70%',
    marginTop: 10,
    marginBottom: 20,
    borderRadius: 10
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700'
  },
  loadingText: {
    fontSize: 18
  },
  activityIndicator: {
    marginTop: height * .35,
    height: height * .2,
    transform: [{scale: 2}]
  }
})
