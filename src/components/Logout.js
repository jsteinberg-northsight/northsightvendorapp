import React, { Component } from 'react'
import { View } from 'react-native'
import { logout } from '../api/auth'

export default class LogoutComponent extends Component {
	
	constructor(props) {
		super(props);
	}

	componentDidMount(){
		logout();
		this.props.navigation.navigate('Login');
	}

	render() {
		return (
			<View></View>
		)
	}
}