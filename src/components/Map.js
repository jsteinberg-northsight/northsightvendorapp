import React, { Component } from 'react';
import { Text, View, SafeAreaView, StyleSheet, Dimensions, PermissionsAndroid, TouchableOpacity, Image, Linking } from 'react-native';
import Modal from 'react-native-modal'
import { StoreGlobal } from '../features/GlobalState';
import { query } from '../api/query';
import { perm } from '../api/permissions';
import MapboxGL from '@react-native-mapbox-gl/maps';
import {lineString as makeLineString} from '@turf/helpers';
import {directionsClient} from '../MapboxClient';

MapboxGL.setAccessToken('pk.eyJ1IjoibnNzb2Z0d2FyZSIsImEiOiJjanplajk2d2YwMWNoM25ucmVuN3UydHMyIn0.uit2cJe5JduJQwykR2mJVA');

const {width, height} = Dimensions.get('window');
const iconStyles = {	
	icon: {
		iconImage: ['get', 'icon'],
		iconAllowOverlap: true,
		iconSize: (height * .06) / 240,
		iconOpacity: 1,
		iconColor: '#42aacd',
		textField: '{textField}',
		textAnchor: 'bottom',
		textColor: 'white',
		textAllowOverlap: true,
		symbolAvoidEdges: true,
		iconIgnorePlacement: true,
	}
};
const GrassIcon = require('../../assets/images/grassicon.png');
const GrassAndShrubIcon = require('../../assets/images/grassandshrubicon.png');

export default class MapComponent extends Component {

	constructor() {
		super();
		this.state = {
			userLocation: [-98.5795, 39.8283],
			centerCoordinate: [-98.5795, 39.8283],
			selectedMarker: MapboxGL.geoUtils.makeFeature({type: 'Point', coordinates: [0, 0], layerIndex: 0},  
					{ icon: 'grass', layerIndex: 0, due: '', key: 9999, latitude: 0, longitude: 0, pay: 0, textField: "$0", title: '', address: '', driveTime: ''}),
			flexState: 0,
			locationFound: false,
			modalVisible: false,
			secondModalVisible: false,
			markerModalVisible: false,
			filters: ['grass', 'shrub'],
			filtersSet: false,
			coordinates: MapboxGL.geoUtils.makeFeatureCollection(),
			json: MapboxGL.geoUtils.makeFeatureCollection()
		};
		//this.filterMarkers = this.filterMarkers.bind(this);
	}

	componentDidMount = async () => {
		setTimeout(()=>this.setState({flexState: 1}),1);
	}

	onRegionChange = async (region) => {
		await this._getProperties();
	}

	setModalVisible = () => {
		this.setState({modalVisible: !this.state.modalVisible});
	}

	setSecondModalVisible = () => {
		this.setState({secondModalVisible: !this.state.secondModalVisible});
	}

	setMarkerModalVisible = () => {
		this.setState({markerModalVisible: !this.state.markerModalVisible});
	}

	_getCurrentLocation = async () => {
		var perms = await perm(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
		if (perms === PermissionsAndroid.RESULTS.GRANTED) {
			navigator.geolocation.getCurrentPosition((position) => {
				var lat = parseFloat(position.coords.latitude);
				var long = parseFloat(position.coords.longitude);
	
				this.setState({ centerCoordinate: [long + .0005, lat + .0005], locationFound: true });
				this.setState({ centerCoordinate: [long, lat], locationFound: true, userLocation: [long, lat] });
			},
				(error) => alert("Location not found, please make sure it is enabled and try again."),
				{ enableHighAccuracy: true, timeout: 30000, maximumAge: 5000 });
		}
		else {
			alert('Permissions for location must be granted to use this feature.');
		}
	}

	_getProperties = async (e) => {
		const zoom = await this._map.getZoom();
		if (zoom.valueOf() > 4){
			if (!this.state.locationFound)
				await this._getCurrentLocation();
			var mapBounds = await this._map.getVisibleBounds();

			if (mapBounds !== undefined) {

				var queryString = StoreGlobal({type:'get',key:'MapQueryString'}).replace("${mapBounds.northEast.latitude}", mapBounds[0][1]).replace("${mapBounds.southWest.latitude}", mapBounds[1][1]).replace("${mapBounds.northEast.longitude}", mapBounds[0][0]).replace("${mapBounds.southWest.longitude}", mapBounds[1][0]);

				var response = await query(queryString, 'admin');

				var orderMarkers = MapboxGL.geoUtils.makeFeatureCollection();
				var payouts = [];

				for (let i = 0; i < response.records.length; i++) {
					var due = new Date(new Date(response.records[i].Scheduled_Date__c).toLocaleDateString());
					due.setDate(due.getDate() + 1);
					var pay = due < new Date(new Date().toLocaleDateString()) ? 30 : due.toLocaleDateString() == new Date().toLocaleDateString() ? 25 : 20;
					MapboxGL.geoUtils.addToFeatureCollection(orderMarkers, MapboxGL.geoUtils.makeFeature({type: 'Point', coordinates: [response.records[i].Geocode_Cache__r.Location__Longitude__s, response.records[i].Geocode_Cache__r.Location__Latitude__s], layerIndex: i},  
					{ icon: response.records[i].Work_Ordered__c.toLowerCase().indexOf('grass') > -1 ? 'grass' : 'grassandshrub', layerIndex: i, due: response.records[i].Scheduled_Date__c, key: response.records[i].Id, latitude: response.records[i].Geocode_Cache__r.Location__Latitude__s, 
						longitude: response.records[i].Geocode_Cache__r.Location__Longitude__s, pay: pay, textField: "$" + pay, title: response.records[i].Work_Ordered__c, 
						address: response.records[i].Geocode_Cache__r.Formatted_Street_Address__c, driveTime: ''}));
				}
				
				StoreGlobal({
					type: 'set',
					key: 'payouts',
					value: payouts
				});
				this.setState({json: orderMarkers});
				//this.filterMarkers();
			}
		}
	}

	_reassignOrder = async (e) => {
		alert(`Order ${e.key} assigned`);
		this._removeItem(e.index);
	}

	filterMarkers = () => {
		const list = this.state.fullMarkers;
		const filterList = this.state.filters;
		var newList = list.filter(function (el) {
			if (filterList.includes('grass') && el.title.toLowerCase().indexOf('grass') > -1)
				return (el.title)
			else if (filterList.includes('shrub') && el.title.toLowerCase().indexOf('shrub') > -1)
				return (el.title)
			// else if (filterList.includes('inspection') && el.title.toLowerCase().indexOf('inspection') > -1)
			// 	return (el.title)
			// else if (filterList.includes('maid') && el.title.toLowerCase().indexOf('maid') > -1)
			// 	return (el.title)
		});
		this.setState({ markers: newList });
	}

	_removeItem(index) {
		const list = this.state.markers;
		list.splice(index, 1);
		this.setState({ markers: list });
	}

	setFilter = (filter) => {
		const filterList = this.state.filters;
		if (!filterList.includes(filter))
			filterList.push(filter);
		else{
			var filterIndex = filterList.findIndex(obj => obj === filter)
			filterList.splice(filterIndex, 1);
		}
		this.setState({ filters: filterList });
		this.filterMarkers();
	}

	onLogoutPress = () => {
		this.props.navigation.navigate('Logout');
	}

	onNavigationPress = () => {
		var lat = this.state.selectedMarker.properties.latitude;
		var lng = this.state.selectedMarker.properties.longitude;
		var url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=" + lat + "," + lng;
		Linking.canOpenURL(url).then(supported => {
			if (!supported) {
				console.log('Can\'t handle url: ' + url);
			} else {
				return Linking.openURL(url);
			}
		}).catch(err => console.error('An error occurred', err)); 
	}

	onSourceLayerPress = async(e) => {
		const feature = e.nativeEvent.payload;
		try {
			const reqOptions = {
				waypoints: [
				  {coordinates: [this.state.userLocation[0], this.state.userLocation[1]]},
				  {coordinates: [feature.geometry.coordinates[0], feature.geometry.coordinates[1]]},
				],
				profile: 'driving',
				geometries: 'geojson',
				steps: true,
				bannerInstructions: true,
				voiceInstructions: true,
			  };
		  
			const res = await directionsClient.getDirections(reqOptions).send();
			var lineString = makeLineString(res.body.routes[0].geometry.coordinates);
			feature.properties.driveTime = (res.body.routes[0].duration / 60).toFixed(1) + ' min';
			this.setState({centerCoordinate: [feature.geometry.coordinates[0], feature.geometry.coordinates[1]], coordinates: lineString, selectedMarker: feature});
			this.setMarkerModalVisible();
		} catch (error) {
			alert(error);
		}
	}

	render() {
		//let IconComponent = Ionicons;
		return (
			<SafeAreaView style={{flex: 1}}>
				<MapboxGL.MapView
					style={{flex: 1}}
					styleURL={'mapbox://styles/nssoftware/cjziuuz8a02421cmvjny5xkqp'}
					ref={map => { this._map = map; }}
					onRegionDidChange={this.onRegionChange}
					logoEnabled={false}
					compassEnabled={false}
					rotateEnabled={false}
					onLayout={this._getCurrentLocation}
					showUserLocation={true}
					attributionEnabled={false}
					>
					<MapboxGL.Camera
						ref={camera => { this._camera = camera; }}
						zoomLevel={12}
						centerCoordinate={this.state.centerCoordinate}
						animationIn={'flyTo'}
					/>

					<MapboxGL.UserLocation />

					<MapboxGL.Images
						images={{grass: GrassIcon, grassandshrub: GrassAndShrubIcon}}
						sdf={true}
					/>

					<MapboxGL.ShapeSource
						id="workorders"
						shape={this.state.json}
						onPress={this.onSourceLayerPress}
					>
						<MapboxGL.SymbolLayer
							id="symbolLocationSymbols"
							minZoomLevel={1}
							style={iconStyles.icon}
							/>
					</MapboxGL.ShapeSource>
					<MapboxGL.ShapeSource
						id="polylineShape"
						shape={this.state.coordinates}>
					<MapboxGL.LineLayer
						id={'polylineLine'}
						style={{
							visibility: 'visible',						
							lineColor: '#334DC7',
							lineWidth: 6
						}} />						
					</MapboxGL.ShapeSource>
					
					</MapboxGL.MapView>
					<View style={styles.calloutViewTop} >
						<TouchableOpacity style={styles.topButton} onPress={this.setSecondModalVisible}>
							<Image resizeMode={'contain'} style={styles.topButton} source={require('../../assets/images/profileicon.png')} />
						</TouchableOpacity>
					</View>
					<View style={styles.calloutViewBottom} >
						<TouchableOpacity style={styles.button} onPress={this._getCurrentLocation}>
							<Image resizeMode={'contain'} style={styles.button} source={require('../../assets/images/mapreseticon.png')} />
						</TouchableOpacity>
					</View>
					<Modal
						animationIn="slideInUp"
						animationOut="slideOutDown"
						visible={this.state.modalVisible}
						onBackButtonPress={this.setModalVisible}
						onBackdropPress={this.setModalVisible}
						onSwipeComplete={this.setModalVisible}
						coverScreen={true}
						hasBackdrop={true}
						backdropOpacity={.7}>
						<View style={{marginTop: height * .75, alignItems: 'center', justifyContent: 'center', height: height / 4, width: width * .9, backgroundColor: 'white'}}>
							<View>
								<TouchableOpacity style={{flex: 1, flexDirection: 'row', width: width, justifyContent: "center"}} onPress={this.setModalVisible}>
									<Text>Filters &#8595;</Text>
								</TouchableOpacity>
								<View style={{flex: 1, flexDirection: 'row'}}>
									<TouchableOpacity style={{width: '30%', alignItems: "center"}} onPress={() => this.setFilter('grass')}>
										<Text>Grass</Text>
									</TouchableOpacity>
									<TouchableOpacity style={{width: '30%', alignItems: "center"}} onPress={() => this.setFilter('maid')}>
										<Text>Maid</Text>
									</TouchableOpacity>
									<TouchableOpacity style={{width: '30%', alignItems: "center"}} onPress={() => this.setFilter('inspection')}>
										<Text>Inspection</Text>
									</TouchableOpacity>
								</View>								
							</View>
						</View>
					</Modal>
					<Modal
						animationIn="slideInLeft"
						animationOut="slideOutRight"
						visible={this.state.secondModalVisible}
						onBackButtonPress={this.setSecondModalVisible}
						onBackdropPress={this.setSecondModalVisible}
						onSwipeComplete={this.setSecondModalVisible}
						coverScreen={true}
						hasBackdrop={true}
						backdropOpacity={.7}>
						{/* <View style={{flexDirection: 'row', height: height * .3, width: width * .7, marginLeft: width * .3, backgroundColor: 'white'}}>
							<TouchableOpacity style={styles.topButton} onPress={this.setSecondModalVisible}>
								<Image resizeMode={'contain'} style={styles.topButton} source={require('../../assets/images/profileicon.png')} />
							</TouchableOpacity>
							<Text style={{marginTop: height * .14}}>Erick Molina</Text>
						</View> */}
						<View style={{flexDirection: 'row', width: width * .7, marginLeft: width * .3, height: height * .3, justifyContent: 'space-around', backgroundColor: 'white'}}>
							<TouchableOpacity onPress={this.setSecondModalVisible}>
								<Image resizeMode={'contain'} style={{height: height * .1, width: height * .1, marginLeft: width * .02, marginTop: height * .1}} source={require('../../assets/images/profileicon.png')} />
							</TouchableOpacity>
							<View style={{flexDirection: 'column', alignContent: 'center', marginRight: width * .02, marginTop: height * .13}}>
								<Text>Erick Molina</Text>
							</View>
						</View>
						<View style={{height: height * .7, marginTop: 0, width: width * .7, marginLeft: width * .3, backgroundColor: 'rgba(147, 153, 163, 0.8)'}}>
							{/* <Text style={styles.menuText}>Profile</Text>
							<Text style={styles.menuText}>Preferences</Text>
							<Text style={styles.menuText}>About</Text>
							<Text style={styles.menuText}>Contact Support</Text> */}
							<TouchableOpacity onPress={this.onLogoutPress}>
								<Text style={styles.menuText}>Logout</Text>
							</TouchableOpacity>
						</View>
					</Modal>
					<Modal
						animationIn="slideInUp"
						animationOut="slideOutDown"
						visible={this.state.markerModalVisible}
						onBackButtonPress={this.setMarkerModalVisible}
						onBackdropPress={this.setMarkerModalVisible}
						onSwipeComplete={this.setMarkerModalVisible}
						coverScreen={true}
						hasBackdrop={true}
						backdropOpacity={.7}>
						<View style={{marginTop: height * .69, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center', height: height * .3, width: width * .9, borderRadius: 10}}>
							<View>
								<View style={{flexDirection: 'row', width: width, justifyContent: 'space-around'}}>
									<Image style={styles.largebutton} source={this.state.selectedMarker.properties.title.toLowerCase().indexOf('grass') > -1 ? require( '../../assets/images/grasslarge.png') : require('../../assets/images/grassandshrublarge.png')} />
									<View style={{flexDirection: 'column', alignContent: 'center', marginRight: width * .02, marginTop: height * .03}}>
										<Text style={{fontSize: 15}}>Work Order: {this.state.selectedMarker.properties.title}</Text>
										<Text style={{fontSize: 15}}>Payout: {this.state.selectedMarker.properties.textField}</Text>
										<Text style={{fontSize: 15}}>Drive Time: {this.state.selectedMarker.properties.driveTime}</Text>
									</View>
								</View>
								<TouchableOpacity style={{flexDirection: 'row', marginLeft: width * .1, width: width * .8, height: height * .06, borderRadius: 10, bottom: height * .02, justifyContent: 'center', alignContent: 'center', alignItems: 'center', backgroundColor: '#42aacd'}} onPress={this.onNavigationPress}>
									<Text style={{color: 'white', fontSize: 15, fontWeight: 'bold', alignSelf: 'center'}}>Start</Text>
								</TouchableOpacity>
							</View>
						</View>
					</Modal>
			</SafeAreaView>
		);
	}
}

const mapStyle = [];

const styles = StyleSheet.create({
	infowindow: {
		backgroundColor: '#fff',
		//padding: 5,
		borderRadius: 4,
		width: width,
		// textAlign: 'center',
		// alignItems: 'center',
		height: height * .35,
		marginTop: height * .65
	},
	circle: {		
		height: height * .06,
		width: height * .06,
		borderRadius: (height * .06) / 2,
		borderWidth: 2,
		backgroundColor: 'white',
		alignItems: 'center',
		justifyContent: 'center'
	},
	paycircle: {
		width: height * .03,
		height: height * .03,
		marginTop: (height * .03),
		//left: height * .01,
		textAlign: "center",
		justifyContent: 'center'
	},
	pinTail: {
		width: 0,
		height: 0,
		marginLeft: height * .02,
		marginTop: -2,
		backgroundColor: 'transparent',
		borderStyle: 'solid',
		borderLeftWidth: 7,
		borderRightWidth: 7,
		borderBottomWidth: 25,
		borderLeftColor: 'transparent',
		borderRightColor: 'transparent',
		transform: [{rotate: '180deg'}]
	},
	pinText: {
		fontWeight: 'bold',
		textAlign: 'center',
		fontSize: 12,
		color: 'white'
	},
	calloutViewTop: {
		backgroundColor: "rgba(255, 255, 255, 0.9)",
		borderRadius: 22.5,
		marginTop: height * .03,
		zIndex: 10,
		marginLeft: width * .85,
		width: height * .07,
		position: "absolute"
	},
	calloutViewBottom: {
		backgroundColor: "rgba(255, 255, 255, 0.9)",
		borderRadius: 15,
		marginBottom: height * .02,
		zIndex: 10,
		marginLeft: width * .85,
		marginTop: height * .85,
		position: "absolute",
		height: height * .06,
		width: height * .06,
		borderRadius: (height * .06) / 2,
	},
	topButton: {
		height: height * .07,
		alignSelf: 'center',
		alignItems: 'center'
	},
	button: {
		height: height * .06,
		width: height * .06,
		borderRadius: (height * .06) / 2,
		alignSelf: 'center',
		alignItems: 'center'
	},
	largebutton: {
		height: height * .13,
		width: height * .13,
		borderRadius: (height * .15) / 2,
		alignSelf: 'center',
		alignItems: 'center',
		marginBottom: height * .05,
		marginLeft: width * .02
	},
	menuText: {
		color: 'white',
		marginLeft: 5,
		marginTop: 2,
		fontFamily: 'Roboto'
	},
})