import { createBottomTabNavigator, createStackNavigator } from 'react-navigation'
import React from 'react'
import LogoutComponent from '../components/Logout'
import MapComponent from '../components/Map'
import AssignedComponent from '../components/Assigned'
import DetailsComponent from '../components/Details'

// const OrderRouteNavigator = createStackNavigator({
//   Assigned: {
//     screen: AssignedComponent
//   },
//   Details: {
//     screen: DetailsComponent
//   }
// },
// { headerMode: 'none' },
// { initialRouteName: 'Assigned' });

const LoggedInNavigator = createStackNavigator({  
  Market: {
    screen: MapComponent
  },
  Logout: {
    screen: LogoutComponent
  }
},
{ headerMode: 'none' },
{ initialRouteName: 'Market' });

// const LoggedInNavigator = createBottomTabNavigator({
//   Logout: LogoutComponent,
//   Orders: OrderRouteNavigator,
//   Market: MapComponent,
// }, {

//   defaultNavigationOptions: ({ navigation }) => ({
//     tabBarIcon: ({ focused, horizontal, tintColor }) => {
//       const { routeName } = navigation.state;
//       let IconComponent = Ionicons;
//       let iconName;
//       if (routeName === 'Logout') {
//         iconName = `md-body`;
//       } else if (routeName === 'Orders') {
//         iconName = `ios-list`;
//       } else if (routeName === 'Market') {
//         iconName = `ios-pin`;
//       }
      
//       if (iconName !== null)
//         return <IconComponent name={iconName} size={25} color={tintColor} />;
//       else
//         return null
//     },
//   }),
//   tabBarOptions: {
//     activeTintColor: '#2980b6',
//     inactiveTintColor: 'gray',
//   },
// });

export default LoggedInNavigator