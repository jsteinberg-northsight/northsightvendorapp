import { createStackNavigator } from 'react-navigation'

import Login from '../components/Login'

const LoggedOutNavigator = createStackNavigator({
  Login: {
    screen: Login
  }
},
{ headerMode: 'none' },
{ initialRouteName: 'Login' });

export default LoggedOutNavigator